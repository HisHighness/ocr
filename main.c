#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "image.h"
#include "rotation.h"
#include "grayscale.h"

void use()
{
    printf("Usage : ./ocr [args] [angle] [img]\n");
    printf("-g	: transforms the image to grayscale\n");
    printf("-r X : operates a rotation of X degrees on img\n");
}

int main(int argc, char* argv[])
{
    int gflag = 0;
    int rflag = 0;
    char *angle = NULL;
    char *out = "img.bmp";
    char *img = NULL;
    int c;

    while ((c = getopt(argc, argv, "gr:")) != -1)
    {
        switch(c)
        {
            case 'g':
                gflag = 1;
                break;
            case 'r':
                rflag = 1;
                angle = optarg;
                break;
            case '?':
                use();
                return 1;
            default:
                return 1;
        }
    }

    SDL_Surface *image = NULL;
    if (optind < argc)
    {
        img = argv[optind];
        image = IMG_Load(img);
    }
    if (gflag)
        image = grayscale(image);

    if (rflag)
    {
        if (angle)
            image = rotate(image,atoi(angle));
    }

    if (image)
        SDL_SaveBMP(image,out);
    return 0;
}
