CC=gcc
CFLAGS=-Wall -Wextra --std=c99 -lm
LDFLAGS=-lSDL_image `sdl-config --libs --cflags`
EXEC=ocr

all: $(EXEC)

ocr: main.o image.o rotation.o grayscale.o
	$(CC) -ggdb -o  $@ $^ $(LDFLAGS) $(CFLAGS)

clean: 
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
