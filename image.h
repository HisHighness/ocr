Uint32 getPixel (SDL_Surface *surface, int x, int y);
void setPixel (SDL_Surface *surface, int x, int y, Uint32 pixel);
void lockSurface (SDL_Surface *surface);
void unlockSurface (SDL_Surface *surface);
SDL_Surface *empty_surface(int w, int h);
