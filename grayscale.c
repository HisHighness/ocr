#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "image.h"

/* Niveau de gris */

SDL_Surface *grayscale(SDL_Surface *image)
{
    Uint8 r,g,b;
    Uint32 pixel;
    Uint32 color;

    lockSurface(image);
    for (int x = 0; x < image->w; x++)
    {
        for (int y = 0; y < image->h; y++)
        {
            pixel = getPixel(image, x, y);
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            r = r*0.2126 + g*0.7152 + b*0.0722;
            color = SDL_MapRGB(image->format, r, r, r);
            setPixel(image, x, y, color);
        }
    }
    unlockSurface(image);

    return image;
}
