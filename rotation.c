#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include "image.h"

/* ROTATION BITCHES §§§§§§§§§ (Seulement pour la soutenance 2) */

SDL_Surface *rotate(SDL_Surface *image, int angle)
{
    /* Trigo */
    double c = cos(3.14159*(angle)/180);
    double s = sin(3.14159*(angle)/180);

    /* Dimensions de l'imagine d'origine */
    int w = image->w;
    int h = image->h;

    /* Nouvelles dimensions post rotation */
    int wo = abs(w*c) + abs(h*s);
    int ho = abs(h*c) + abs(w*s);

    /* On évite de complètement pourrir l'image d'origine avec une nouvelle
     * image adaptée aux nouvelles dimensions post rotation */
    SDL_Surface *new_img;
    new_img = empty_surface(wo,ho);

    /* Milieu de l'image d'origine */
    int ocx = w/2;
    int ocy = h/2;

    /* Milieu de l'image post rotation */
    int ncx = wo/2;
    int ncy = ho/2;

    Uint8 r,g,b;
    Uint32 pixel;
    Uint32 color;

    /* "image" et "new_img" sont lock afin de pouvoir écrire dessus */
    lockSurface(image);
    lockSurface(new_img);
    for (int y = 0; y < ho; y++)
    {
        for(int x = 0; x < wo; x++)
        {
            /* Nouvelles coordonnées post rotation (rotation par rapport au
             * milieu */
            int xo = ocx + (c*(x - ncx) + s*(y - ncy));
            int yo = ocy + (c*(y - ncy) - s*(x - ncx));

            if (xo >= 0 && yo >= 0 && xo < image->w && yo < image->h)
            {
                pixel = getPixel(image, xo, yo);
                SDL_GetRGB(pixel,image->format,&r,&g,&b);
                color = SDL_MapRGB(new_img->format,r,g,b);
                setPixel(new_img,x,y,color);
            }
        }
    }
    /* Une fois la rotation terminée on libère les deux images */
    unlockSurface(image);
    unlockSurface(new_img);

    return new_img;
}
