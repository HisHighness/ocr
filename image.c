#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>

Uint32 getPixel (SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16 *)p;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            return *(Uint32 *)p;
            break;

        default:
            return 0;
    }
}

void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            } else {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

/* Toutes les surfaces n'ont pas besoin d'être lock pour opérer dessus (cf
 * le Wiki de la SDL), d'où la création d'une nouvelle fonction afin de ne lock
 * la surface que si MUSTLOCK retourne 1 */
void lockSurface(SDL_Surface *surface)
{
    if (SDL_MUSTLOCK(surface))
        SDL_LockSurface(surface);
}

/* Écrire SDL_UnlockSurface à chaque fois c'est chiant, donc flemme. */

void unlockSurface(SDL_Surface *surface)
{
    SDL_UnlockSurface(surface);
}

/* Création d'une surface vide, flemme réecrire à chaque fois rmask, gmask etc
 * et SDL_CreateRGB blabla */

SDL_Surface *empty_surface (int w, int h)
{
    SDL_Surface *empty;
    Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    empty = SDL_CreateRGBSurface(0,w,h,32,rmask,gmask,bmask,amask);
    return empty;
}
